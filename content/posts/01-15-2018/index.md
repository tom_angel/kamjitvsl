---
title: "První příspevěk"
cover: ""
author: "andel"
date: "01/15/2018"
category: "tech"
tags:
    - Secondlife
    - vse
    - uvodnik
    - editorial
---
# Zrod Rozcestníku KamJitvSL

## Úvod

Prvním impulsem pro tento web bylo zavření projektu  secondlife.cz
v roce cca  2014-15 původními autory, od té doby nebyl nikde seznam míst s českým popisem,
proto bych vám všem představil nový pokus o rozcestník.

 ## Hledáme spolupracovníky

 Tento web není jen má aktivita a doufám že se přidají talentovaní fotografové Podělte se  s ostatními o místech v Second Life.
## Co bude v dalších článcích ?

Takže co je v plánu, toulky po všech kontinentech, recenze zajimavých míst a doufám noví reportéři.
