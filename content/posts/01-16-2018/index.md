---
title: "Zima v Mořici"
cover: ""
author: "andel"
date: "01/16/2018"
category: "tech"
tags:
    - Secondlife
    - vse
    - zima
---
# Zima v Mořici

Tak v novem roce začneme toulky sněhem v Secondlife a navštivíme jedno zimní středisko zřízené lindeny.
![](./1.jpg)
Startoval jsem z jednoho letiště ještě za krásného  počasí,ale  přistál sem málem do závěje ale díky místní rolbě sem měl  odmetenou dráhu.
![](./2.jpg)
[LM na letiště](http://maps.secondlife.com/secondlife/Valmorel/166/115/96)
No a pak jsem se vydal do  Mořice zimního střediska Linden.Musím řící, že snežný kontinent je fajn hlavně v létě, ale o vánocích má své kouzlo
![](./3.jpg)
Zima v Secondlife je kouzelná celou cestou  míjíme krásný zimní osady.
![](./4.jpg)
Po odpočinku v Chatě Linden jsem oblékl brusle a relaxoval na bruslenim na jednom z mistnich jezer.
![](./5.jpg)
Po krásném bruslení jsem si oblékl lyže a lanovkou se nechal vyvézt na horu Linden Lyže najdete u lanovky.
![](./6.jpg)
Lanovka mě vyvezla na vrchol, odkud byl rozhled  na celé zimní středisko.
![](./7.jpg)
Tímto naše zimní reportáž končí a teším se na komentáře a připadně náměty na další reportáže.
[LM](http://maps.secondlife.com/secondlife/Wengen/15/243/86)
