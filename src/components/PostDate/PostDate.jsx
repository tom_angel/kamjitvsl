import React from "react";
import moment from "moment";


import "./PostDate.css";

class PostDate extends React.Component {
  render() {
    const { date } = this.props;
    return (
      <time
        className="post-date"
        dateTime={moment(new Date(date)).locale("cs").format("D.M.YYYY")}
      >
        {moment(new Date(date)).locale("cs").format("D.M.YYYY")}
      </time>
    );
  }
}

export default PostDate;
