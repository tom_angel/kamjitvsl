import React from "react";
import PaginationLink from "../PaginationLink/PaginationLink";
import "./Pagination.css";

class Pagination extends React.Component {
  render() {
    const { page, pages, prev, next } = this.props;
    return (
      <nav className="pagination">
        <PaginationLink
          className="newer-posts"
          url={prev}
          text="← Novější příspěvky"
        />
        <span className="page-number">
          Stránka {page} z {pages}
        </span>
        <PaginationLink
          className="older-posts"
          url={next}
          text="Starší příspěvky →"
        />
      </nav>
    );
  }
}

export default Pagination;
